# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2018 BroadTech IT Solutions Pvt Ltd 
#    (<http://broadtech-innovations.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError

class ApiUrlConfig(models.Model):
    _name = "api.url.config"
    _description = 'API URL Configuration'
    
    name = fields.Selection([('Production', 'Production'), ('Test', 'Test')], 'Type', default='Test')
    url = fields.Char('URL', default='https://test.carjam.co.nz/api/car/')
    key = fields.Char('Key', default='6D3D54CF4FBE43C903D8CF6513713E4BE5345619')
    
    @api.model
    def create(self, vals):
        conf_id = self.search([])
        if conf_id:
            raise UserError(_("Not able to configure more than 1 data."))
        res = super(ApiUrlConfig, self).create(vals)
        return res
    
    
class VindecoderAPI(models.Model):
    _name = "vindecoder.api"
    _description = 'Vindecoder API Configuration'
    _rec_name = 'apikey'
    
    url = fields.Char('URL', default='https://api.vindecoder.eu/2.0/')
    apikey = fields.Char('API Key', default='1b171eab497b')
    secretkey = fields.Char('Secret Key', default='78d3678bcb')
    
    @api.model
    def create(self, vals):
        conf_id = self.search([])
        if conf_id:
            raise UserError(_("Not able to configure more than 1 data."))
        res = super(VindecoderAPI, self).create(vals)
        return res
 
 