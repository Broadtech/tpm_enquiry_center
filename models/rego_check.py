# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2018 BroadTech IT Solutions Pvt Ltd 
#    (<http://broadtech-innovations.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import api, fields, models, tools, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError

import requests
import logging
import json
import hashlib
logger = logging.getLogger(__name__)

class TpmRegoCheck(models.Model):
    _name = "tpm.rego.check"
    _description = 'TPM Rego Check'
    
    name = fields.Char('Plate Number')
    vin_no = fields.Char('VIN No')
    frame_no = fields.Char('Frame No')
    year = fields.Char('Year')
    make = fields.Char('Make')
    model = fields.Char('Model')
    manufacturer = fields.Char('Manufacturer')
    plant_country = fields.Char('Plant Country')
    product_type = fields.Char('Product Type')
    manufacturer_address = fields.Text('Manufacturer Address')
    check_digit = fields.Char('Check Digit')
    sequential_no = fields.Char('Sequential No')
    body = fields.Char('Body')
    drive = fields.Char('Drive')
    num_seats = fields.Integer('Number of Seats')
    num_doors = fields.Integer('Number of Doors')
    num_airbags = fields.Integer('Number of Airbags')
    colour = fields.Char('Colour')
    production_date = fields.Date('Production Date')
    engine_ccm = fields.Char('Engine ccm')
    engine_cylinders = fields.Integer('Engine Cylinders')
    transmission = fields.Char('Transmission')
    num_gears = fields.Integer('Number of Gears')
    engine_full = fields.Char('Engine (full)')
    fuel_type = fields.Char('Fuel Type - Primary')
    engine_power = fields.Char('Engine Power')
    transmission_full = fields.Char('Transmission (full)')
    steering = fields.Char('Steering')
    emission_standard = fields.Char('Emission Standard')
    a_c = fields.Char('A/C')
    interior_fabric = fields.Char('Interior Fabric')
    equipment = fields.Text('Equipment')
#     year = fields.Selection([(num, str(num)) for num in range(1990, (datetime.now().year)+1 )], 'Year')
    
    
    def action_get_vindata(self):
        self.ensure_one()
        if self.vin_no:
            config_id = self.env['vindecoder.api'].search([])
            if not config_id:
                raise UserError(_("Please configure Vindecoder API"))
            id = self.vin_no
            apikey = config_id.apikey
            secretkey = config_id.secretkey
            controlsum = id+'|'+apikey+'|'+secretkey
            sha = hashlib.sha1(controlsum.encode('utf-8')).hexdigest()[0:10]
            url = config_id.url+apikey+'/'+sha+'/decode/'+id+'.json'
            try:
                response = requests.get(url, params={})
                data = response.json()
                if 'decode' in data and data['decode']:
                    for val in data['decode']:
                        if val['label'] == 'Model Year':
                            self.year = val['value']
                        if val['label'] == 'Make':
                            self.make = val['value']
                        if val['label'] == 'Model':
                            self.model = val['value']
                        if val['label'] == 'Manufacturer':
                            self.manufacturer = val['value']
                        if val['label'] == 'Plant Country':
                            self.plant_country = val['value']
                        if val['label'] == 'Product Type':
                            self.product_type = val['value']
                        if val['label'] == 'Manufacturer Address':
                            self.manufacturer_address = val['value']
                        if val['label'] == 'Check Digit':
                            self.check_digit = val['value']
                        if val['label'] == 'Sequential Number':
                            self.sequential_no = val['value']
                        if val['label'] == 'Body':
                            self.body = val['value']
                        if val['label'] == 'Drive':
                            self.drive = val['value']   
                        if val['label'] == 'Number of Seats':
                            self.num_seats = val['value']
                        if val['label'] == 'Number of Doors':
                            self.num_doors = val['value']
                        if val['label'] == 'Transmission':
                            self.transmission = val['value']
                        if val['label'] == 'Number of Gears':
                            self.num_gears = val['value']
                        if val['label'] == 'Steering':
                            self.steering = val['value']
                        if val['label'] == 'Made':
                            self.production_date = val['value']
            except Exception:
                logger.exception("Failed to fetch the url %r", url)
#                 raise ValidationError(_("Failed to fetch the URL %s")% url)
                return None
        return True
    
    def action_get_data(self):
        self.ensure_one()
        config_id = self.env['api.url.config'].search([])
        if not config_id:
            raise UserError(_("Please configure Carjam API"))
        url = config_id.url+'?plate='+self.name+'&key='+config_id.key+'&amp;basic=1&f=json'
        try:
            response = requests.get(url, params={})
            data = response.json()
            if 'idh' in data and data['idh'] and 'vehicle' in data['idh'] and data['idh']['vehicle']:
                if 'vin' in data['idh']['vehicle']:
                    self.vin_no = data['idh']['vehicle']['vin']
                if 'year_of_manufacture' in data['idh']['vehicle']:
                    self.year = data['idh']['vehicle']['year_of_manufacture']
                if 'make' in data['idh']['vehicle']:
                    self.make = data['idh']['vehicle']['make']
                if 'model' in data['idh']['vehicle']:
                    self.model = data['idh']['vehicle']['model']
                if 'country_of_origin' in data['idh']['vehicle']:
                    self.plant_country = data['idh']['vehicle']['country_of_origin']
                if 'vehicle_type' in data['idh']['vehicle']:
                    self.product_type = data['idh']['vehicle']['vehicle_type']
                if 'body_style' in data['idh']['vehicle']:
                    self.body = data['idh']['vehicle']['body_style']
                if 'no_of_seats' in data['idh']['vehicle']:
                    self.num_seats = data['idh']['vehicle']['no_of_seats']
                if 'main_colour' in data['idh']['vehicle']:
                    self.colour = data['idh']['vehicle']['main_colour']
                if 'fuel_type' in data['idh']['vehicle']:
                    self.fuel_type = data['idh']['vehicle']['fuel_type']
                if 'power' in data['idh']['vehicle']:
                    self.engine_power = data['idh']['vehicle']['power']
        except Exception:
            logger.exception("Failed to fetch the url %r", url)
            raise ValidationError(_("Failed to fetch the URL %s")% url)
            return None
        if self.vin_no:
            self.action_get_vindata()
        return True
    
    
    
